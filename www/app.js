var canvas, ctx, $div, $div2, pos, akt=false, farbtotal='#000000';

window.onerror = function(err) {
    alert(err);
}

var  Position = function(x, y){
     this.x = x;
     this.y = y;
}

var  startzeichnen = function (e) {
   akt= true;
   pos = new Position(e.touches[0].clientX, e.touches[0].clientY-45);
   
};

var zeichnen = function(e){

    if(akt) {
        ctx.beginPath();
        ctx.strokeStyle = farbtotal;
        ctx.lineWidth = 2;
        ctx.moveTo(pos.x, pos.y);
        ctx.lineTo(e.touches[0].clientX, e.touches[0].clientY-45);
        ctx.stroke();
        pos = new Position(e.touches[0].clientX, e.touches[0].clientY-45);
    } 
}

var endezeichnen = function (e){
    akt=false;
    pos = new Position(0, 0);
    ctx.closePath();
}

var speichernBild = function() {

    window.canvas2ImagePlugin.saveImageDataToLibrary (
        function() {alert('Daten gespeichert')},
        function() {alert('Probleme beim Speichern')},
        canvas

    );

}


var fotografieren = function() {


    navigator.camera.getPicture(
        function(image){
            var bild = new Image();
            bild.src = image;
            bild.onload = function() {
            ctx.drawImage(bild,0,0)
            }
        },
        function(){
            alert('fehler');
        },
        {
            targetWidth:200,
            targetHeight:200,
            correctOrientation:true

        }
    );


}

var farbenmachen = function(farben) {

    for (i=0; i<farben.length; i++) {
       
        let  color=farben[i];
        $('<button>').appendTo($div2).
                    addClass('but').
                    css('background-color', color).
                    on('click', function() { farbtotal=color;
                                            $div.css('background-color', color);
                                            $div2.css('background-color', color)});


    }

}


document.addEventListener('deviceready', function(){
    var farben =[];
    $.ajax({

        url: 'http://wifi.1av.at/colors.php',
        method: 'get',
        success:function(response) {
           
            farben = response.colors;
            //JSON
            //{colors:[...]}
          /*  for (var i=0; i<response.colors.length; i++) {
                    farben.push(response.colors[i]);
            } */
            farbenmachen(farben);
            
        }




    })
   // var farben = ['#ff0000', '#008000', '#0000ff', '#000000'];

    $div = $('<div>').prependTo('body').addClass('div1');
    canvas = document.getElementById('canvas');
    canvas.setAttribute('width', window.innerWidth-20);
    canvas.setAttribute('height', window.innerHeight-100);
  

    canvas.addEventListener('touchstart', function(e) { startzeichnen(e);});
    canvas.addEventListener('touchmove', function(e) { zeichnen(e);});
    canvas.addEventListener('touchend', function(e) { endezeichnen(e);});
    ctx = canvas.getContext("2d");
    $div2 = $('<div>').appendTo('body').addClass('div1');

    


   $('<button>').appendTo($div).addClass('but2').html('Kamera').on('click', function(){ fotografieren();})
   $('<button>').appendTo($div).addClass('but2').html('Löschen').on('click', function(){ ctx.clearRect(0, 0, canvas.width, canvas.height);})
   $('<button>').appendTo($div).addClass('but2').html('Speichern').on('click', function(){ speichernBild();})

console.log('Device ready');
});