window.onerror = function( err ) {
    alert( err );
}

class SuperPaint {
     constructor() {
        this.drawModus = 'line'; //'dot';
        this.dotSize = 3;
        this.color = '#000';
        this.ctx = false;        
        this.init();
     }

     initPens() {
        
        var _this = this;

        $.ajax({

            

            url: 'http://wifi.1av.at/colors.php',
            method: 'get',
            success:function(response) {
               
                // this ist nicht mehr this!! > this ist das Ajax

                var colors = response.colors;
                for (let i in colors ) {
                    $( '<pen>' )
                        .css({background:colors[i]})
                        .on('click', function(e) { _this.activatePen(e,colors[i]) }.bind(_this) )
                        .appendTo( 'footer' )
                        .addClass(  i == 0 ? 'active' : '' );
                    }
                
            }
        })

       
        
     }  
     
     activatePen( e,newColor ) {
       if ( $(e.currentTarget).hasClass( 'active') ) {
            $( 'footer' ).addClass( 'show' );
            $( 'pen.active' ).removeClass( 'active' );
            $( '#paint' ).css( 'pointer-events','none');
       } else {
            $( 'footer' ).removeClass( 'show' );
            this.color = newColor;
            $( '#paint' ).css( 'pointer-events','');
            $(e.currentTarget).addClass( 'active' );
       }
      }

     initCanvas() {
        let c = $( '#paint' );
        c.attr({
            width:$(window).width(),
            height:$(window).height() - $('h1').height() - 10
        });
        c.on( 'touchstart', this.drawStart.bind(this) );
        c.on( 'touchmove', this.draw.bind(this) );
        this.ctx = c.get(0).getContext( '2d' );
     }

     init() {
        this.initPens();
        this.initCanvas();
        this.initButtons();
     }

     drawStart(e) {      
        var offset = $('#paint').offset();
        this.oldX = e.touches[0].pageX;
        this.oldY =  e.touches[0].pageY - offset.top;
        this.draw(e);
     }

     draw(e) {
        var offset = $('#paint').offset();
        var x = e.touches[0].pageX;
        var y = e.touches[0].pageY - offset.top;
        this.ctx.fillStyle =  this.ctx.strokeStyle =  this.color;
        this.ctx.lineWidth = this.dotSize;
        this.ctx.beginPath();
        switch( this.drawModus ) {
            case 'dot':
                this.ctx.arc(x,y,this.dotSize,0,2*Math.PI);
                this.ctx.fill();
                break;
            case 'line':
                this.ctx.moveTo(this.oldX,this.oldY);
                this.ctx.lineTo(x,y);
                this.ctx.stroke();
                this.oldX = x;
                this.oldY = y;
                break;

        }
       
     }
     
     initButtons() {
         $( 'a' ).on( 'click', function(e){
           switch( $(e.currentTarget).attr( 'href' )) {
               case '#cam':


                   // alert( 'öffne Kamera' );
                   navigator.camera.getPicture(
                       function( image ) {
                           alert( image );
                            var bild = new Image();
                            bild.src = image;
                            bild.onload = function() {
                                alert( 'bild geladen ')
                                var ctx = $( '#paint' ).get(0).getContext('2d');
                                ctx.drawImage(bild,0,0)
                                alert( ctx );
                            }

                           
                       },
                       function() {
                            alert( 'fehler' );
                       },
                       {
                           targetWidth:200,
                           targetHeight:200,
                           correctOrientation:true
                       }
                    );

                   break;
                case '#save':
                    window.canvas2ImagePlugin.saveImageDataToLibrary(
                        function() { alert('Daten gespeichert!') },
                        function() { alert( 'Probleme beim Speichern')},
                        $('#paint').get(0)
                    );
                    break;
                case '#clear':
                    $( 'pen.active' ).removeClass( 'active' );
                    $( 'pen:first' ).trigger( 'click' );
                    this.ctx.clearRect(0,0,1000,1000);
                    break;
           }
         }.bind(this) );
     }
}





document.addEventListener( 'deviceready', function() {

   
   $( '#bar' ).css('left',0); // show nav bar
   var sp = new SuperPaint(); 
   

   $( window ).on( 'resize', function() {
        sp.initCanvas();
        $( 'a:last' ).trigger( 'click' );
   }) 
});

